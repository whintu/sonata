from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    class Meta:
        db_table = 'profile'
        verbose_name = 'профиль пользователя'
        verbose_name_plural = 'профили пользователей'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    org = models.ForeignKey("Org", on_delete=models.DO_NOTHING)
    position = models.ForeignKey("Position", on_delete=models.DO_NOTHING)

    def __str__(self):
        # return f'{self.user.first_name} {self.user.last_name}'
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Event(models.Model):
    class Meta:
        db_table = 'event_set'
        verbose_name = 'мероприятие'
        verbose_name_plural = 'мероприятия'

    id = models.AutoField(db_column='event_id', verbose_name='идентификатор мероприятия', primary_key=True)
    name = models.TextField(db_column='event_name', verbose_name='наменование мероприятия')
    full_name = models.TextField(db_column='event_full_name', verbose_name='полное наименование мероприятия', blank=True)
    action_list = models.ForeignKey("ActionList", on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.name} ({self.org_role}: {self.org})'


class ActionList(models.Model):
    class Meta:
        db_table = 'action_list_set'
        verbose_name = 'набор действий над типами документов'
        verbose_name_plural = 'наборы действий над типами документов'

    id = models.AutoField(db_column='action_list_id', verbose_name='идентификатор', primary_key=True)
    name = models.CharField(db_column='action_list_name', max_length=80, verbose_name='наименование')
    role = models.ForeignKey("OrgRole", on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'


class Action(models.Model):
    class Meta:
        db_table = 'action'
        verbose_name = 'действие'
        verbose_name_plural = 'действия'

    id = models.AutoField(db_column='action_id', verbose_name='действие', primary_key=True)
    name = models.CharField(db_column='action_name', max_length=100, verbose_name='наименование действия')
    actionlist = models.ForeignKey("ActionList", on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'


class OrgRole(models.Model):
    class Meta:
        db_table = 'role'
        verbose_name = 'роль'
        verbose_name_plural = 'роли'

    id = models.AutoField(db_column='role_id', verbose_name='идентификатор', primary_key=True)
    name = models.TextField(db_column='role_name', verbose_name='наименование роли')
    position = models.ForeignKey("Position", on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.name}'


class Position(models.Model):
    class Meta:
        db_table = 'position_set'
        verbose_name = 'должность'
        verbose_name_plural = 'должности'

    id = models.AutoField(db_column='position_id', verbose_name='идентификатор', primary_key=True)
    name = models.TextField(db_column='position_name', verbose_name='наименование должности')
    org = models.ForeignKey("Org", on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'


class EventResult(models.Model):
    class Meta:
        db_table = 'event_result'
        verbose_name = 'результат мероприятия'
        verbose_name_plural = 'результаты мероприятий'

    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    income_number = models.TextField(db_column='event_result', verbose_name='входящий номер', blank=True)
    outcome_number = models.TextField(db_column='outcome_number', verbose_name='исходящий', blank=True)

    def __str__(self):
        return f'{self.event} | исх. № {self.income_number} | вх. № {self.outcome_number}'


class AdmArea(models.Model):
    class Meta:
        db_table = 'adm_area_set'
        verbose_name = 'административный округ'
        verbose_name_plural = 'административные округа'
    
    id = models.AutoField(db_column='adm_area_id', verbose_name='идентификатор', primary_key=True)
    name = models.TextField(db_column='adm_area_name', verbose_name='наименование')
    okato = models.CharField(max_length=11, blank=True)

    def __str__(self):
        return self.name


class District(models.Model):
    class Meta:
        db_table = 'district_set'
        verbose_name = 'Муниципальный район'
        verbose_name_plural = 'Муниципальные районы'

    id = models.AutoField(db_column='district_id', verbose_name='идентификатор', primary_key=True)
    name = models.TextField(db_column='district_name', verbose_name='наименование')
    oktmo = models.CharField(max_length=11, blank=True)

    def __str__(self):
        return self.name


class Org(models.Model):
    class Meta:
        db_table = 'org_set'
        verbose_name = 'огранизация'
        verbose_name_plural = 'организации'
    
    unk = models.AutoField(db_column='org_id', verbose_name='идентификатор организации', primary_key=True)
    name = models.TextField(db_column='org_name', verbose_name='наименование организации')
    full_name = models.TextField(db_column='org_full_name', verbose_name='полное наименование организации')
    adm_area = models.ForeignKey('AdmArea', blank=True, on_delete=models.DO_NOTHING)
    district = models.ForeignKey('District', blank=True, on_delete=models.DO_NOTHING)
    postal_code = models.TextField(db_column='postal_code', blank=True, verbose_name='почтовый индекс')
    fact_address = models.TextField(db_column='fact_address', blank=True, verbose_name='адрес')

    def __str__(self):
        return self.name


class Doc(models.Model):
    class Meta:
        db_table = 'doc_set'
        verbose_name = 'документ'
        verbose_name_plural = 'документы'
    
    id = models.AutoField(db_column='doc_id', verbose_name='идентификатор документа', primary_key=True)
    name = models.TextField(db_column='doc_name', verbose_name='наименование документа')
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    org = models.ForeignKey('Org', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} [{self.event}]'
