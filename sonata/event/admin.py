from django.contrib import admin

from .models import Profile, ActionList, Action, OrgRole, Position, EventResult, AdmArea, District, Org, Doc, Event


from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin


class ProfileInline(admin.StackedInline):
    model = Profile
    max_num = 1
    can_delete = False


class UserAdmin(AuthUserAdmin):
    inlines = [ProfileInline]


# unregister old user admin
admin.site.unregister(User)
# register new user admin
admin.site.register(User, UserAdmin)


class ActionInline(admin.StackedInline):
    model = Action

class ActionListAdmin(admin.ModelAdmin):
    inlines = [ActionInline]


class OrgRoleAdmin(admin.ModelAdmin):
    pass


class PositionAdmin(admin.ModelAdmin):
    pass


class EventResultAdmin(admin.ModelAdmin):
    pass


class AdmAreaAdmin(admin.ModelAdmin):
    pass


class DistrictAdmin(admin.ModelAdmin):
    pass


class OrgAdmin(admin.ModelAdmin):
    pass


class DocAdmin(admin.ModelAdmin):
    pass

class EventAdmin(admin.ModelAdmin):
    pass


admin.site.register(ActionList, ActionListAdmin)
admin.site.register(OrgRole, OrgRoleAdmin)
admin.site.register(Position, PositionAdmin)
admin.site.register(EventResult, EventResultAdmin)
admin.site.register(AdmArea, AdmAreaAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(Org, OrgAdmin)
admin.site.register(Doc, DocAdmin)
admin.site.register(Event, EventAdmin)
