from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404, HttpResponseRedirect


@login_required(login_url='/login')
def index(request):
    return render(request, 'about.html', locals())


def about(request):
    return render(request, 'about.html', locals())


def doc_processing(request):
    return render(request, 'event/doc-processing.html', locals())


def grbs_kur(request):
    return render(request, 'event/grbs-kur.html', locals())


def grbs_ruk(request):
    return render(request, 'event/grbs-ruk.html', locals())


def pbs_buh(request):
    return render(request, 'event/pbs-buh.html', locals())


def pbs_ruk(request):
    return render(request, 'event/pbs-ruk.html', locals())
