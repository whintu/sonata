--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11 (Ubuntu 10.11-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.11 (Ubuntu 10.11-1.pgdg18.04+1)

-- Started on 2019-12-01 18:38:35 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 16526)
-- Name: admin; Type: SCHEMA; Schema: -; Owner: uiuser
--

CREATE SCHEMA admin;


ALTER SCHEMA admin OWNER TO uiuser;

--
-- TOC entry 11 (class 2615 OID 18561)
-- Name: import; Type: SCHEMA; Schema: -; Owner: sonata
--

CREATE SCHEMA import;


ALTER SCHEMA import OWNER TO sonata;

--
-- TOC entry 1 (class 3079 OID 13039)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3241 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 2 (class 3079 OID 16480)
-- Name: xml2; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS xml2 WITH SCHEMA public;


--
-- TOC entry 3242 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION xml2; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION xml2 IS 'XPath querying and XSLT';


--
-- TOC entry 229 (class 1259 OID 17037)
-- Name: adm_area_set_adm_area_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.adm_area_set_adm_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.adm_area_set_adm_area_id_seq OWNER TO uiuser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 217 (class 1259 OID 16662)
-- Name: auth_group; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE admin.auth_group OWNER TO uiuser;

--
-- TOC entry 216 (class 1259 OID 16660)
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.auth_group_id_seq OWNER TO uiuser;

--
-- TOC entry 3243 (class 0 OID 0)
-- Dependencies: 216
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.auth_group_id_seq OWNED BY admin.auth_group.id;


--
-- TOC entry 219 (class 1259 OID 16672)
-- Name: auth_group_permissions; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE admin.auth_group_permissions OWNER TO uiuser;

--
-- TOC entry 218 (class 1259 OID 16670)
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.auth_group_permissions_id_seq OWNER TO uiuser;

--
-- TOC entry 3244 (class 0 OID 0)
-- Dependencies: 218
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.auth_group_permissions_id_seq OWNED BY admin.auth_group_permissions.id;


--
-- TOC entry 215 (class 1259 OID 16654)
-- Name: auth_permission; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE admin.auth_permission OWNER TO uiuser;

--
-- TOC entry 214 (class 1259 OID 16652)
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.auth_permission_id_seq OWNER TO uiuser;

--
-- TOC entry 3245 (class 0 OID 0)
-- Dependencies: 214
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.auth_permission_id_seq OWNED BY admin.auth_permission.id;


--
-- TOC entry 221 (class 1259 OID 16680)
-- Name: auth_user; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE admin.auth_user OWNER TO uiuser;

--
-- TOC entry 223 (class 1259 OID 16690)
-- Name: auth_user_groups; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE admin.auth_user_groups OWNER TO uiuser;

--
-- TOC entry 222 (class 1259 OID 16688)
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.auth_user_groups_id_seq OWNER TO uiuser;

--
-- TOC entry 3246 (class 0 OID 0)
-- Dependencies: 222
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.auth_user_groups_id_seq OWNED BY admin.auth_user_groups.id;


--
-- TOC entry 220 (class 1259 OID 16678)
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.auth_user_id_seq OWNER TO uiuser;

--
-- TOC entry 3247 (class 0 OID 0)
-- Dependencies: 220
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.auth_user_id_seq OWNED BY admin.auth_user.id;


--
-- TOC entry 225 (class 1259 OID 16698)
-- Name: auth_user_user_permissions; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE admin.auth_user_user_permissions OWNER TO uiuser;

--
-- TOC entry 224 (class 1259 OID 16696)
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.auth_user_user_permissions_id_seq OWNER TO uiuser;

--
-- TOC entry 3248 (class 0 OID 0)
-- Dependencies: 224
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.auth_user_user_permissions_id_seq OWNED BY admin.auth_user_user_permissions.id;


--
-- TOC entry 230 (class 1259 OID 17048)
-- Name: district_set_district_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.district_set_district_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.district_set_district_id_seq OWNER TO uiuser;

--
-- TOC entry 227 (class 1259 OID 16758)
-- Name: django_admin_log; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE admin.django_admin_log OWNER TO uiuser;

--
-- TOC entry 226 (class 1259 OID 16756)
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.django_admin_log_id_seq OWNER TO uiuser;

--
-- TOC entry 3249 (class 0 OID 0)
-- Dependencies: 226
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.django_admin_log_id_seq OWNED BY admin.django_admin_log.id;


--
-- TOC entry 213 (class 1259 OID 16644)
-- Name: django_content_type; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE admin.django_content_type OWNER TO uiuser;

--
-- TOC entry 212 (class 1259 OID 16642)
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.django_content_type_id_seq OWNER TO uiuser;

--
-- TOC entry 3250 (class 0 OID 0)
-- Dependencies: 212
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.django_content_type_id_seq OWNED BY admin.django_content_type.id;


--
-- TOC entry 211 (class 1259 OID 16565)
-- Name: django_migrations; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE admin.django_migrations OWNER TO uiuser;

--
-- TOC entry 210 (class 1259 OID 16563)
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.django_migrations_id_seq OWNER TO uiuser;

--
-- TOC entry 3251 (class 0 OID 0)
-- Dependencies: 210
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.django_migrations_id_seq OWNED BY admin.django_migrations.id;


--
-- TOC entry 228 (class 1259 OID 16789)
-- Name: django_session; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE admin.django_session OWNER TO uiuser;

--
-- TOC entry 231 (class 1259 OID 17059)
-- Name: doc_set_doc_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.doc_set_doc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.doc_set_doc_id_seq OWNER TO uiuser;

--
-- TOC entry 257 (class 1259 OID 18747)
-- Name: profile; Type: TABLE; Schema: admin; Owner: uiuser
--

CREATE TABLE admin.profile (
    id integer NOT NULL,
    org_id integer NOT NULL,
    position_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE admin.profile OWNER TO uiuser;

--
-- TOC entry 256 (class 1259 OID 18745)
-- Name: event_profile_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.event_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.event_profile_id_seq OWNER TO uiuser;

--
-- TOC entry 3252 (class 0 OID 0)
-- Dependencies: 256
-- Name: event_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: admin; Owner: uiuser
--

ALTER SEQUENCE admin.event_profile_id_seq OWNED BY admin.profile.id;


--
-- TOC entry 232 (class 1259 OID 17064)
-- Name: event_set_event_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.event_set_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.event_set_event_id_seq OWNER TO uiuser;

--
-- TOC entry 234 (class 1259 OID 17092)
-- Name: org_role_set_org_role_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.org_role_set_org_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.org_role_set_org_role_id_seq OWNER TO uiuser;

--
-- TOC entry 233 (class 1259 OID 17081)
-- Name: org_set_org_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.org_set_org_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.org_set_org_id_seq OWNER TO uiuser;

--
-- TOC entry 235 (class 1259 OID 17103)
-- Name: org_type_set_org_type_id_seq; Type: SEQUENCE; Schema: admin; Owner: uiuser
--

CREATE SEQUENCE admin.org_type_set_org_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin.org_type_set_org_type_id_seq OWNER TO uiuser;

--
-- TOC entry 237 (class 1259 OID 18585)
-- Name: action; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.action (
    action_id integer NOT NULL,
    action_name character varying(100) NOT NULL,
    actionlist_id integer NOT NULL
);


ALTER TABLE public.action OWNER TO uiuser;

--
-- TOC entry 236 (class 1259 OID 18583)
-- Name: action_action_id_seq; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.action_action_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.action_action_id_seq OWNER TO uiuser;

--
-- TOC entry 3253 (class 0 OID 0)
-- Dependencies: 236
-- Name: action_action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.action_action_id_seq OWNED BY public.action.action_id;


--
-- TOC entry 239 (class 1259 OID 18596)
-- Name: action_list_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.action_list_set (
    action_list_id integer NOT NULL,
    action_list_name character varying(80) NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.action_list_set OWNER TO uiuser;

--
-- TOC entry 238 (class 1259 OID 18594)
-- Name: action_list_set_action_list_id_seq; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.action_list_set_action_list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.action_list_set_action_list_id_seq OWNER TO uiuser;

--
-- TOC entry 3254 (class 0 OID 0)
-- Dependencies: 238
-- Name: action_list_set_action_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.action_list_set_action_list_id_seq OWNED BY public.action_list_set.action_list_id;


--
-- TOC entry 241 (class 1259 OID 18607)
-- Name: adm_area_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.adm_area_set (
    adm_area_id integer NOT NULL,
    adm_area_name text NOT NULL,
    okato character varying(11) NOT NULL
);


ALTER TABLE public.adm_area_set OWNER TO uiuser;

--
-- TOC entry 240 (class 1259 OID 18605)
-- Name: adm_area_set_adm_area_id_seq1; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.adm_area_set_adm_area_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.adm_area_set_adm_area_id_seq1 OWNER TO uiuser;

--
-- TOC entry 3255 (class 0 OID 0)
-- Dependencies: 240
-- Name: adm_area_set_adm_area_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.adm_area_set_adm_area_id_seq1 OWNED BY public.adm_area_set.adm_area_id;


--
-- TOC entry 207 (class 1259 OID 16440)
-- Name: counties; Type: TABLE; Schema: public; Owner: sonata
--

CREATE TABLE public.counties (
    wkt character varying,
    name character varying,
    okato character varying,
    abbrev character varying
);


ALTER TABLE public.counties OWNER TO sonata;

--
-- TOC entry 243 (class 1259 OID 18618)
-- Name: district_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.district_set (
    district_id integer NOT NULL,
    district_name text NOT NULL,
    oktmo character varying(11) NOT NULL
);


ALTER TABLE public.district_set OWNER TO uiuser;

--
-- TOC entry 242 (class 1259 OID 18616)
-- Name: district_set_district_id_seq1; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.district_set_district_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.district_set_district_id_seq1 OWNER TO uiuser;

--
-- TOC entry 3256 (class 0 OID 0)
-- Dependencies: 242
-- Name: district_set_district_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.district_set_district_id_seq1 OWNED BY public.district_set.district_id;


--
-- TOC entry 255 (class 1259 OID 18684)
-- Name: doc_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.doc_set (
    doc_id integer NOT NULL,
    doc_name text NOT NULL,
    event_id integer NOT NULL,
    org_id integer NOT NULL
);


ALTER TABLE public.doc_set OWNER TO uiuser;

--
-- TOC entry 254 (class 1259 OID 18682)
-- Name: doc_set_doc_id_seq1; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.doc_set_doc_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doc_set_doc_id_seq1 OWNER TO uiuser;

--
-- TOC entry 3257 (class 0 OID 0)
-- Dependencies: 254
-- Name: doc_set_doc_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.doc_set_doc_id_seq1 OWNED BY public.doc_set.doc_id;


--
-- TOC entry 208 (class 1259 OID 16522)
-- Name: doc_spr; Type: TABLE; Schema: public; Owner: sonata
--

CREATE TABLE public.doc_spr (
);


ALTER TABLE public.doc_spr OWNER TO sonata;

--
-- TOC entry 253 (class 1259 OID 18673)
-- Name: event_result; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.event_result (
    id integer NOT NULL,
    event_result text NOT NULL,
    outcome_number text NOT NULL,
    event_id integer NOT NULL
);


ALTER TABLE public.event_result OWNER TO uiuser;

--
-- TOC entry 252 (class 1259 OID 18671)
-- Name: event_result_id_seq; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.event_result_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_result_id_seq OWNER TO uiuser;

--
-- TOC entry 3258 (class 0 OID 0)
-- Dependencies: 252
-- Name: event_result_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.event_result_id_seq OWNED BY public.event_result.id;


--
-- TOC entry 245 (class 1259 OID 18629)
-- Name: event_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.event_set (
    event_id integer NOT NULL,
    event_name text NOT NULL,
    event_full_name text NOT NULL,
    action_list_id integer NOT NULL
);


ALTER TABLE public.event_set OWNER TO uiuser;

--
-- TOC entry 244 (class 1259 OID 18627)
-- Name: event_set_event_id_seq1; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.event_set_event_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_set_event_id_seq1 OWNER TO uiuser;

--
-- TOC entry 3259 (class 0 OID 0)
-- Dependencies: 244
-- Name: event_set_event_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.event_set_event_id_seq1 OWNED BY public.event_set.event_id;


--
-- TOC entry 203 (class 1259 OID 16386)
-- Name: grbs; Type: TABLE; Schema: public; Owner: sonata
--

CREATE TABLE public.grbs (
    grbs_id integer,
    grbs_name text,
    is_grbs integer,
    is_gad integer,
    is_gaifd integer,
    is_creator integer
);


ALTER TABLE public.grbs OWNER TO sonata;

--
-- TOC entry 3260 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN grbs.grbs_id; Type: COMMENT; Schema: public; Owner: sonata
--

COMMENT ON COLUMN public.grbs.grbs_id IS 'код ведомства';


--
-- TOC entry 3261 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN grbs.grbs_name; Type: COMMENT; Schema: public; Owner: sonata
--

COMMENT ON COLUMN public.grbs.grbs_name IS 'наименование ведомства';


--
-- TOC entry 3262 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN grbs.is_grbs; Type: COMMENT; Schema: public; Owner: sonata
--

COMMENT ON COLUMN public.grbs.is_grbs IS 'является ГРБС';


--
-- TOC entry 3263 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN grbs.is_gad; Type: COMMENT; Schema: public; Owner: sonata
--

COMMENT ON COLUMN public.grbs.is_gad IS 'является ГАД';


--
-- TOC entry 3264 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN grbs.is_gaifd; Type: COMMENT; Schema: public; Owner: sonata
--

COMMENT ON COLUMN public.grbs.is_gaifd IS 'является ГАИФД';


--
-- TOC entry 3265 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN grbs.is_creator; Type: COMMENT; Schema: public; Owner: sonata
--

COMMENT ON COLUMN public.grbs.is_creator IS 'является учредителем';


--
-- TOC entry 204 (class 1259 OID 16392)
-- Name: org; Type: TABLE; Schema: public; Owner: sonata
--

CREATE TABLE public.org (
    unk integer NOT NULL,
    inn text,
    full_name text,
    short_name text,
    department_code text,
    org_type text,
    okato text,
    oktmo text,
    adm_area text,
    district text,
    postal_code text,
    fact_address text,
    personal_accounts text
);


ALTER TABLE public.org OWNER TO sonata;

--
-- TOC entry 206 (class 1259 OID 16432)
-- Name: org_lk; Type: TABLE; Schema: public; Owner: sonata
--

CREATE TABLE public.org_lk (
    unk integer,
    lk_number text NOT NULL,
    lk_date_open date,
    lk_date_close date
);


ALTER TABLE public.org_lk OWNER TO sonata;

--
-- TOC entry 247 (class 1259 OID 18640)
-- Name: org_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.org_set (
    org_id integer NOT NULL,
    org_name text NOT NULL,
    org_full_name text NOT NULL,
    postal_code text NOT NULL,
    fact_address text NOT NULL,
    adm_area_id integer NOT NULL,
    district_id integer NOT NULL
);


ALTER TABLE public.org_set OWNER TO uiuser;

--
-- TOC entry 246 (class 1259 OID 18638)
-- Name: org_set_org_id_seq1; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.org_set_org_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.org_set_org_id_seq1 OWNER TO uiuser;

--
-- TOC entry 3266 (class 0 OID 0)
-- Dependencies: 246
-- Name: org_set_org_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.org_set_org_id_seq1 OWNED BY public.org_set.org_id;


--
-- TOC entry 249 (class 1259 OID 18651)
-- Name: position_set; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.position_set (
    position_id integer NOT NULL,
    position_name text NOT NULL,
    org_id integer NOT NULL
);


ALTER TABLE public.position_set OWNER TO uiuser;

--
-- TOC entry 248 (class 1259 OID 18649)
-- Name: position_set_position_id_seq; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.position_set_position_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.position_set_position_id_seq OWNER TO uiuser;

--
-- TOC entry 3267 (class 0 OID 0)
-- Dependencies: 248
-- Name: position_set_position_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.position_set_position_id_seq OWNED BY public.position_set.position_id;


--
-- TOC entry 205 (class 1259 OID 16414)
-- Name: result; Type: TABLE; Schema: public; Owner: sonata
--

CREATE TABLE public.result (
    id integer NOT NULL,
    name text,
    "in" text,
    "out" text
);


ALTER TABLE public.result OWNER TO sonata;

--
-- TOC entry 209 (class 1259 OID 16536)
-- Name: result_id_seq; Type: SEQUENCE; Schema: public; Owner: sonata
--

CREATE SEQUENCE public.result_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.result_id_seq OWNER TO sonata;

--
-- TOC entry 3268 (class 0 OID 0)
-- Dependencies: 209
-- Name: result_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sonata
--

ALTER SEQUENCE public.result_id_seq OWNED BY public.result.id;


--
-- TOC entry 251 (class 1259 OID 18662)
-- Name: role; Type: TABLE; Schema: public; Owner: uiuser
--

CREATE TABLE public.role (
    role_id integer NOT NULL,
    role_name text NOT NULL,
    position_id integer NOT NULL
);


ALTER TABLE public.role OWNER TO uiuser;

--
-- TOC entry 250 (class 1259 OID 18660)
-- Name: role_role_id_seq; Type: SEQUENCE; Schema: public; Owner: uiuser
--

CREATE SEQUENCE public.role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_role_id_seq OWNER TO uiuser;

--
-- TOC entry 3269 (class 0 OID 0)
-- Dependencies: 250
-- Name: role_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uiuser
--

ALTER SEQUENCE public.role_role_id_seq OWNED BY public.role.role_id;


--
-- TOC entry 2983 (class 2604 OID 16665)
-- Name: auth_group id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group ALTER COLUMN id SET DEFAULT nextval('admin.auth_group_id_seq'::regclass);


--
-- TOC entry 2984 (class 2604 OID 16675)
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('admin.auth_group_permissions_id_seq'::regclass);


--
-- TOC entry 2982 (class 2604 OID 16657)
-- Name: auth_permission id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_permission ALTER COLUMN id SET DEFAULT nextval('admin.auth_permission_id_seq'::regclass);


--
-- TOC entry 2985 (class 2604 OID 16683)
-- Name: auth_user id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user ALTER COLUMN id SET DEFAULT nextval('admin.auth_user_id_seq'::regclass);


--
-- TOC entry 2986 (class 2604 OID 16693)
-- Name: auth_user_groups id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('admin.auth_user_groups_id_seq'::regclass);


--
-- TOC entry 2987 (class 2604 OID 16701)
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('admin.auth_user_user_permissions_id_seq'::regclass);


--
-- TOC entry 2988 (class 2604 OID 16761)
-- Name: django_admin_log id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_admin_log ALTER COLUMN id SET DEFAULT nextval('admin.django_admin_log_id_seq'::regclass);


--
-- TOC entry 2981 (class 2604 OID 16647)
-- Name: django_content_type id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_content_type ALTER COLUMN id SET DEFAULT nextval('admin.django_content_type_id_seq'::regclass);


--
-- TOC entry 2980 (class 2604 OID 16568)
-- Name: django_migrations id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_migrations ALTER COLUMN id SET DEFAULT nextval('admin.django_migrations_id_seq'::regclass);


--
-- TOC entry 3000 (class 2604 OID 18750)
-- Name: profile id; Type: DEFAULT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.profile ALTER COLUMN id SET DEFAULT nextval('admin.event_profile_id_seq'::regclass);


--
-- TOC entry 2990 (class 2604 OID 18588)
-- Name: action action_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.action ALTER COLUMN action_id SET DEFAULT nextval('public.action_action_id_seq'::regclass);


--
-- TOC entry 2991 (class 2604 OID 18599)
-- Name: action_list_set action_list_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.action_list_set ALTER COLUMN action_list_id SET DEFAULT nextval('public.action_list_set_action_list_id_seq'::regclass);


--
-- TOC entry 2992 (class 2604 OID 18610)
-- Name: adm_area_set adm_area_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.adm_area_set ALTER COLUMN adm_area_id SET DEFAULT nextval('public.adm_area_set_adm_area_id_seq1'::regclass);


--
-- TOC entry 2993 (class 2604 OID 18621)
-- Name: district_set district_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.district_set ALTER COLUMN district_id SET DEFAULT nextval('public.district_set_district_id_seq1'::regclass);


--
-- TOC entry 2999 (class 2604 OID 18687)
-- Name: doc_set doc_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.doc_set ALTER COLUMN doc_id SET DEFAULT nextval('public.doc_set_doc_id_seq1'::regclass);


--
-- TOC entry 2998 (class 2604 OID 18676)
-- Name: event_result id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.event_result ALTER COLUMN id SET DEFAULT nextval('public.event_result_id_seq'::regclass);


--
-- TOC entry 2994 (class 2604 OID 18632)
-- Name: event_set event_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.event_set ALTER COLUMN event_id SET DEFAULT nextval('public.event_set_event_id_seq1'::regclass);


--
-- TOC entry 2995 (class 2604 OID 18643)
-- Name: org_set org_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.org_set ALTER COLUMN org_id SET DEFAULT nextval('public.org_set_org_id_seq1'::regclass);


--
-- TOC entry 2996 (class 2604 OID 18654)
-- Name: position_set position_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.position_set ALTER COLUMN position_id SET DEFAULT nextval('public.position_set_position_id_seq'::regclass);


--
-- TOC entry 2979 (class 2604 OID 16538)
-- Name: result id; Type: DEFAULT; Schema: public; Owner: sonata
--

ALTER TABLE ONLY public.result ALTER COLUMN id SET DEFAULT nextval('public.result_id_seq'::regclass);


--
-- TOC entry 2997 (class 2604 OID 18665)
-- Name: role role_id; Type: DEFAULT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.role ALTER COLUMN role_id SET DEFAULT nextval('public.role_role_id_seq'::regclass);


--
-- TOC entry 3020 (class 2606 OID 16787)
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- TOC entry 3025 (class 2606 OID 16724)
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- TOC entry 3028 (class 2606 OID 16677)
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3022 (class 2606 OID 16667)
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3015 (class 2606 OID 16710)
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- TOC entry 3017 (class 2606 OID 16659)
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3036 (class 2606 OID 16695)
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 3039 (class 2606 OID 16739)
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- TOC entry 3030 (class 2606 OID 16685)
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3042 (class 2606 OID 16703)
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 3045 (class 2606 OID 16753)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- TOC entry 3033 (class 2606 OID 16781)
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- TOC entry 3048 (class 2606 OID 16767)
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3010 (class 2606 OID 16651)
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- TOC entry 3012 (class 2606 OID 16649)
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- TOC entry 3008 (class 2606 OID 16573)
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 3052 (class 2606 OID 16796)
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- TOC entry 3086 (class 2606 OID 18752)
-- Name: profile event_profile_pkey; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.profile
    ADD CONSTRAINT event_profile_pkey PRIMARY KEY (id);


--
-- TOC entry 3089 (class 2606 OID 18754)
-- Name: profile event_profile_user_id_key; Type: CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.profile
    ADD CONSTRAINT event_profile_user_id_key UNIQUE (user_id);


--
-- TOC entry 3058 (class 2606 OID 18604)
-- Name: action_list_set action_list_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.action_list_set
    ADD CONSTRAINT action_list_set_pkey PRIMARY KEY (action_list_id);


--
-- TOC entry 3056 (class 2606 OID 18593)
-- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT action_pkey PRIMARY KEY (action_id);


--
-- TOC entry 3061 (class 2606 OID 18615)
-- Name: adm_area_set adm_area_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.adm_area_set
    ADD CONSTRAINT adm_area_set_pkey PRIMARY KEY (adm_area_id);


--
-- TOC entry 3006 (class 2606 OID 16448)
-- Name: counties counties_id; Type: CONSTRAINT; Schema: public; Owner: sonata
--

ALTER TABLE ONLY public.counties
    ADD CONSTRAINT counties_id UNIQUE (name);


--
-- TOC entry 3063 (class 2606 OID 18626)
-- Name: district_set district_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.district_set
    ADD CONSTRAINT district_set_pkey PRIMARY KEY (district_id);


--
-- TOC entry 3083 (class 2606 OID 18692)
-- Name: doc_set doc_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.doc_set
    ADD CONSTRAINT doc_set_pkey PRIMARY KEY (doc_id);


--
-- TOC entry 3079 (class 2606 OID 18681)
-- Name: event_result event_result_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.event_result
    ADD CONSTRAINT event_result_pkey PRIMARY KEY (id);


--
-- TOC entry 3066 (class 2606 OID 18637)
-- Name: event_set event_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.event_set
    ADD CONSTRAINT event_set_pkey PRIMARY KEY (event_id);


--
-- TOC entry 3004 (class 2606 OID 16516)
-- Name: org_lk org_lk_pkey; Type: CONSTRAINT; Schema: public; Owner: sonata
--

ALTER TABLE ONLY public.org_lk
    ADD CONSTRAINT org_lk_pkey PRIMARY KEY (lk_number);


--
-- TOC entry 3070 (class 2606 OID 18648)
-- Name: org_set org_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.org_set
    ADD CONSTRAINT org_set_pkey PRIMARY KEY (org_id);


--
-- TOC entry 3002 (class 2606 OID 16450)
-- Name: org pk_org; Type: CONSTRAINT; Schema: public; Owner: sonata
--

ALTER TABLE ONLY public.org
    ADD CONSTRAINT pk_org PRIMARY KEY (unk);


--
-- TOC entry 3073 (class 2606 OID 18659)
-- Name: position_set position_set_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.position_set
    ADD CONSTRAINT position_set_pkey PRIMARY KEY (position_id);


--
-- TOC entry 3075 (class 2606 OID 18670)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (role_id);


--
-- TOC entry 3018 (class 1259 OID 16788)
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_group_name_a6ea08ec_like ON admin.auth_group USING btree (name varchar_pattern_ops);


--
-- TOC entry 3023 (class 1259 OID 16725)
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON admin.auth_group_permissions USING btree (group_id);


--
-- TOC entry 3026 (class 1259 OID 16726)
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON admin.auth_group_permissions USING btree (permission_id);


--
-- TOC entry 3013 (class 1259 OID 16711)
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON admin.auth_permission USING btree (content_type_id);


--
-- TOC entry 3034 (class 1259 OID 16741)
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_user_groups_group_id_97559544 ON admin.auth_user_groups USING btree (group_id);


--
-- TOC entry 3037 (class 1259 OID 16740)
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON admin.auth_user_groups USING btree (user_id);


--
-- TOC entry 3040 (class 1259 OID 16755)
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON admin.auth_user_user_permissions USING btree (permission_id);


--
-- TOC entry 3043 (class 1259 OID 16754)
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON admin.auth_user_user_permissions USING btree (user_id);


--
-- TOC entry 3031 (class 1259 OID 16782)
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX auth_user_username_6821ab7c_like ON admin.auth_user USING btree (username varchar_pattern_ops);


--
-- TOC entry 3046 (class 1259 OID 16778)
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON admin.django_admin_log USING btree (content_type_id);


--
-- TOC entry 3049 (class 1259 OID 16779)
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON admin.django_admin_log USING btree (user_id);


--
-- TOC entry 3050 (class 1259 OID 16798)
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX django_session_expire_date_a5c62663 ON admin.django_session USING btree (expire_date);


--
-- TOC entry 3053 (class 1259 OID 16797)
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX django_session_session_key_c0390e0f_like ON admin.django_session USING btree (session_key varchar_pattern_ops);


--
-- TOC entry 3084 (class 1259 OID 18770)
-- Name: event_profile_org_id_8de51ba3; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX event_profile_org_id_8de51ba3 ON admin.profile USING btree (org_id);


--
-- TOC entry 3087 (class 1259 OID 18771)
-- Name: event_profile_position_id_a43522fc; Type: INDEX; Schema: admin; Owner: uiuser
--

CREATE INDEX event_profile_position_id_a43522fc ON admin.profile USING btree (position_id);


--
-- TOC entry 3054 (class 1259 OID 18780)
-- Name: action_actionlist_id_ba088ac6; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX action_actionlist_id_ba088ac6 ON public.action USING btree (actionlist_id);


--
-- TOC entry 3059 (class 1259 OID 24620)
-- Name: action_list_set_role_id_cae95e80; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX action_list_set_role_id_cae95e80 ON public.action_list_set USING btree (role_id);


--
-- TOC entry 3080 (class 1259 OID 18739)
-- Name: doc_set_event_id_82359491; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX doc_set_event_id_82359491 ON public.doc_set USING btree (event_id);


--
-- TOC entry 3081 (class 1259 OID 18740)
-- Name: doc_set_org_id_905ec151; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX doc_set_org_id_905ec151 ON public.doc_set USING btree (org_id);


--
-- TOC entry 3077 (class 1259 OID 18728)
-- Name: event_result_event_id_524ad2d4; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX event_result_event_id_524ad2d4 ON public.event_result USING btree (event_id);


--
-- TOC entry 3064 (class 1259 OID 18698)
-- Name: event_set_action_list_id_bdb2a2eb; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX event_set_action_list_id_bdb2a2eb ON public.event_set USING btree (action_list_id);


--
-- TOC entry 3067 (class 1259 OID 18709)
-- Name: org_set_adm_area_id_0404b453; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX org_set_adm_area_id_0404b453 ON public.org_set USING btree (adm_area_id);


--
-- TOC entry 3068 (class 1259 OID 18710)
-- Name: org_set_district_id_c9b7c0c4; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX org_set_district_id_c9b7c0c4 ON public.org_set USING btree (district_id);


--
-- TOC entry 3071 (class 1259 OID 18716)
-- Name: position_set_org_id_5c66f281; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX position_set_org_id_5c66f281 ON public.position_set USING btree (org_id);


--
-- TOC entry 3076 (class 1259 OID 18722)
-- Name: role_position_id_13fce07c; Type: INDEX; Schema: public; Owner: uiuser
--

CREATE INDEX role_position_id_13fce07c ON public.role USING btree (position_id);


--
-- TOC entry 3093 (class 2606 OID 16718)
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES admin.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3092 (class 2606 OID 16713)
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES admin.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3091 (class 2606 OID 16704)
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES admin.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3095 (class 2606 OID 16733)
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES admin.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3094 (class 2606 OID 16728)
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES admin.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3097 (class 2606 OID 16747)
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES admin.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3096 (class 2606 OID 16742)
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES admin.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3098 (class 2606 OID 16768)
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES admin.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3099 (class 2606 OID 16773)
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES admin.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3110 (class 2606 OID 18755)
-- Name: profile event_profile_org_id_8de51ba3_fk_org_set_org_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.profile
    ADD CONSTRAINT event_profile_org_id_8de51ba3_fk_org_set_org_id FOREIGN KEY (org_id) REFERENCES public.org_set(org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3111 (class 2606 OID 18760)
-- Name: profile event_profile_position_id_a43522fc_fk_position_set_position_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.profile
    ADD CONSTRAINT event_profile_position_id_a43522fc_fk_position_set_position_id FOREIGN KEY (position_id) REFERENCES public.position_set(position_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3112 (class 2606 OID 18765)
-- Name: profile event_profile_user_id_7d6d33ec_fk_auth_user_id; Type: FK CONSTRAINT; Schema: admin; Owner: uiuser
--

ALTER TABLE ONLY admin.profile
    ADD CONSTRAINT event_profile_user_id_7d6d33ec_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES admin.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3100 (class 2606 OID 18781)
-- Name: action action_actionlist_id_ba088ac6_fk_action_list_set_action_list_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT action_actionlist_id_ba088ac6_fk_action_list_set_action_list_id FOREIGN KEY (actionlist_id) REFERENCES public.action_list_set(action_list_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3101 (class 2606 OID 24621)
-- Name: action_list_set action_list_set_role_id_cae95e80_fk_role_role_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.action_list_set
    ADD CONSTRAINT action_list_set_role_id_cae95e80_fk_role_role_id FOREIGN KEY (role_id) REFERENCES public.role(role_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3109 (class 2606 OID 18729)
-- Name: doc_set doc_set_event_id_82359491_fk_event_set_event_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.doc_set
    ADD CONSTRAINT doc_set_event_id_82359491_fk_event_set_event_id FOREIGN KEY (event_id) REFERENCES public.event_set(event_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3108 (class 2606 OID 18734)
-- Name: doc_set doc_set_org_id_905ec151_fk_org_set_org_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.doc_set
    ADD CONSTRAINT doc_set_org_id_905ec151_fk_org_set_org_id FOREIGN KEY (org_id) REFERENCES public.org_set(org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3107 (class 2606 OID 18723)
-- Name: event_result event_result_event_id_524ad2d4_fk_event_set_event_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.event_result
    ADD CONSTRAINT event_result_event_id_524ad2d4_fk_event_set_event_id FOREIGN KEY (event_id) REFERENCES public.event_set(event_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3102 (class 2606 OID 18693)
-- Name: event_set event_set_action_list_id_bdb2a2eb_fk_action_li; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.event_set
    ADD CONSTRAINT event_set_action_list_id_bdb2a2eb_fk_action_li FOREIGN KEY (action_list_id) REFERENCES public.action_list_set(action_list_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3090 (class 2606 OID 16517)
-- Name: org_lk org_lk_unk_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sonata
--

ALTER TABLE ONLY public.org_lk
    ADD CONSTRAINT org_lk_unk_fkey FOREIGN KEY (unk) REFERENCES public.org(unk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3104 (class 2606 OID 18699)
-- Name: org_set org_set_adm_area_id_0404b453_fk_adm_area_set_adm_area_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.org_set
    ADD CONSTRAINT org_set_adm_area_id_0404b453_fk_adm_area_set_adm_area_id FOREIGN KEY (adm_area_id) REFERENCES public.adm_area_set(adm_area_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3103 (class 2606 OID 18704)
-- Name: org_set org_set_district_id_c9b7c0c4_fk_district_set_district_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.org_set
    ADD CONSTRAINT org_set_district_id_c9b7c0c4_fk_district_set_district_id FOREIGN KEY (district_id) REFERENCES public.district_set(district_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3105 (class 2606 OID 18711)
-- Name: position_set position_set_org_id_5c66f281_fk_org_set_org_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.position_set
    ADD CONSTRAINT position_set_org_id_5c66f281_fk_org_set_org_id FOREIGN KEY (org_id) REFERENCES public.org_set(org_id) DEFERRABLE INITIALLY DEFERRED;


--
-- TOC entry 3106 (class 2606 OID 18717)
-- Name: role role_position_id_13fce07c_fk_position_set_position_id; Type: FK CONSTRAINT; Schema: public; Owner: uiuser
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_position_id_13fce07c_fk_position_set_position_id FOREIGN KEY (position_id) REFERENCES public.position_set(position_id) DEFERRABLE INITIALLY DEFERRED;


-- Completed on 2019-12-01 18:38:38 UTC

--
-- PostgreSQL database dump complete
--

